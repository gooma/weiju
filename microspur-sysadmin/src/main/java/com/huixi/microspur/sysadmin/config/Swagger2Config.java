package com.huixi.microspur.sysadmin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * swagger2配置
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 **/
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Value("${spring.application.name}")
    private String applicationName;
    @Value("${swagger.enabled}")
    private boolean swaggerEnabled;
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swaggerEnabled)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.huixi.microspur.sysadmin.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(getTokenPar());
    }
    /**
     * 实现点：业务系统的token认证机制
     **/
    private List<Parameter> getTokenPar() {
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        tokenPar.name("Authorization")
                .description("认证信息")
                .modelRef(new ModelRef("string")).parameterType("header").required(true).build();
        pars.add(tokenPar.build());
        return pars;
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(applicationName + "接口文档")
                .description(applicationName + "接口文档")
                .contact(new Contact("huixi", "https://gitee.com/huixi_and_their_friends/weiju-wechat", "nb.com"))
                .version("1.0")
                .build();
    }
}
