package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.entity.SysUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
