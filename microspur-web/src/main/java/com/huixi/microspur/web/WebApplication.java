package com.huixi.microspur.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * <p>
 * 启动器
 * </p>
 *
 * @author xzl
 * @since 2020-01-16
 **/
@SpringBootApplication
@MapperScan("com.huixi.microspur.web.mapper")
@EnableCaching
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}
