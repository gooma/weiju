package com.huixi.microspur.web.controller;


import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.entity.Test;
import com.huixi.microspur.web.service.TestService;
import com.huixi.microspur.web.util.RedisUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */

@RestController
@RequestMapping("/test")
public class TestController extends BaseController {


    @Autowired
    TestService testService;

    @Autowired
    RedisUtil redisUtil;


    @ApiOperation("测试查询方法")
    @CachePut(value = "testAll")
    @GetMapping("/testOne")
    public Wrapper test(){

        // 存储进redis
        redisUtil.set("hello", "world", 1);

        Test byId = testService.getById("1");
        System.out.println("GG");
        return WrapMapper.ok(byId);


    }

}

