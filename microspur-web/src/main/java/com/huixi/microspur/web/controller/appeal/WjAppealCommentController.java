package com.huixi.microspur.web.controller.appeal;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageFactory;
import com.huixi.microspur.commons.util.wrapper.WrapMapper;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.entity.VO.WjAppealCommentPageVO;
import com.huixi.microspur.web.entity.appeal.WjAppealComment;
import com.huixi.microspur.web.service.WjAppealCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 诉求-评论 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@RestController
@RequestMapping("/wjAppealComment")
@Api(value = "诉求评论模块")
public class WjAppealCommentController extends BaseController {


    @Autowired
    private WjAppealCommentService wjAppealCommentService;


    /**
     *  添加诉求评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param wjAppealComment
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/addAppealComment")
    @ApiModelProperty(value = "添加诉求评论")
    public Wrapper addAppealComment(@RequestBody WjAppealComment wjAppealComment){

        boolean save = wjAppealCommentService.save(wjAppealComment);

        return WrapMapper.ok();

    }



    /**
     *  删除 诉求评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param wjAppealComment
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/deleteAppealComment")
    @ApiModelProperty(value = "删除 诉求评论")
    public Wrapper deleteAppealComment(@RequestBody WjAppealComment wjAppealComment){

        boolean b = wjAppealCommentService.removeById(wjAppealComment.getAppealCommentId());

        return WrapMapper.ok();

    }


    /**
     *  分页查询诉求的 评论
     * @Author 叶秋
     * @Date 2020/4/13 22:48
     * @param wjAppealCommentPageVO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    public Wrapper queryPageAppealComment(@RequestBody WjAppealCommentPageVO wjAppealCommentPageVO) {

        PageData<Object> objectPageData = new PageData<>();

        Page<WjAppealComment> page = PageFactory.createPage(wjAppealCommentPageVO.getPageQuery());

        QueryWrapper<WjAppealComment> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id", wjAppealCommentPageVO.getWjAppealComment().getAppealId());


        Page<WjAppealComment> page1 = wjAppealCommentService.page(page, objectQueryWrapper);

        BeanUtil.copyProperties(page1, objectPageData);


        return WrapMapper.ok(objectPageData);

    }






}

