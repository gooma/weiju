package com.huixi.microspur.web.entity.VO;

import com.huixi.microspur.web.entity.appeal.WjAppeal;
import com.huixi.microspur.web.entity.appeal.WjAppealMaterial;
import com.huixi.microspur.web.entity.appeal.WjAppealTag;
import com.huixi.microspur.web.entity.user.WjUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *  诉求需要多个表的数据合并才能合成完整数据，故此一个包装
 * @Author 叶秋
 * @Date 2020/3/5 23:11
 * @param
 * @return
 **/
@Data
public class QueryAppealVO extends WjAppeal implements Serializable {

    @ApiModelProperty(value = "是否点赞")
    private Boolean isEndorse;

    @ApiModelProperty(value = "相关用户的信息")
    private WjUser wjUser;

    @ApiModelProperty(value = "诉求相关的素材")
    private List<WjAppealMaterial> appealMaterial;

    @ApiModelProperty(value = "标签的名字（冗余）")
    private List<WjAppealTag> appealTag;

}
