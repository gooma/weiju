package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.VO.ListPageAppealVO;
import com.huixi.microspur.web.entity.appeal.WjAppeal;

import java.util.List;

/**
 * <p>
 * 诉求表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealService extends IService<WjAppeal> {


    /**
     *  按条件分页查询 诉求
     * @Author 叶秋
     * @Date 2020/2/26 21:14
     * @param listPageAppealVO
     * @return java.util.List<com.huixi.microspur.web.entity.appeal.WjAppeal>
     **/
    List<WjAppeal> listPageAppeal(ListPageAppealVO listPageAppealVO);

}
