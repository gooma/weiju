package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.entity.dynamic.WjDynamic;

/**
 * <p>
 * 动态表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicService extends IService<WjDynamic> {

}
