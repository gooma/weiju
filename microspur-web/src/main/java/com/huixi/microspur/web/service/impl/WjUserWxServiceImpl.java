package com.huixi.microspur.web.service.impl;

import cn.hutool.core.codec.Base64;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.commons.enums.ErrorCodeEnum;
import com.huixi.microspur.web.entity.user.WjUser;
import com.huixi.microspur.web.entity.user.WjUserWx;
import com.huixi.microspur.web.entity.user.WxDecodeUserInfoDTO;
import com.huixi.microspur.web.entity.user.WxSesssionKeyDTO;
import com.huixi.microspur.web.mapper.WjUserWxMapper;
import com.huixi.microspur.web.service.WjUserService;
import com.huixi.microspur.web.service.WjUserWxService;
import com.huixi.microspur.web.util.RedisUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjUserWxServiceImpl extends ServiceImpl<WjUserWxMapper, WjUserWx> implements WjUserWxService {



}
